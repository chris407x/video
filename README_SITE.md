This is the bredcrumb guide

This is written in the Yii 2.0 framework:

http://www.yiiframework.com/doc-2.0/guide-index.html

Uses the "advanced" package:

http://www.yiiframework.com/wiki/799/yii2-app-advanced-on-single-domain-apache-nginx/#hh1


The virtual machine is from:

https://github.com/rlerdorf/php7dev

Runs on virtual box

web root in nginx:

 root   /var/www/video/advanced/frontend/web/;
 
 modify conf:
 
sudo vi /etc/nginx/conf.d/video.conf 

restart:

  sudo service nginx reload
  
  Using Apache:
  
  sudo service nginx stop
  sudo apachectl start
  
  
use symlinks:

cd /var/www/video/advanced/frontend/web/
ln -s ../../backend/web backend

Initialize the app:

https://github.com/yiisoft/yii2-app-advanced/blob/master/docs/guide/start-installation.md


 